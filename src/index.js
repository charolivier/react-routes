// index.js
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const ALBUM = [
	{
    "id": 1,
		"file"  : "track1.mp3",
		"title" : "Track 1",
		"image" : "coverart.jpg"
	},
	{
    "id": 2,
		"file"  : "track2.mp3",
		"title" : "Track 2",
		"image" : "coverart.jpg"
	},
	{
    "id": 3,
		"file"  : "track3.mp3",
		"title" : "Track 3",
		"image" : "coverart.jpg"
	}
]

ReactDOM.render(<App album={ALBUM} />, document.getElementById('root'));

registerServiceWorker();
