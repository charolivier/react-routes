import React, { Component } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';

class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (
      <Navbar color="dark" className="navbar-dark navbar-expand-sm" expand="md">
      <div className="container">
        <NavbarBrand tag={Link} to="/">React routes</NavbarBrand>

        <NavbarToggler onClick={this.toggle} />

        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink tag={Link} to="/">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/about/">About</NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/album/">Album</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
              </div>
      </Navbar>
    );
  }
}

export default Header;