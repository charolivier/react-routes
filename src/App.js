// App.js
import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Header from './snippets/Header';

import Home from './views/Home';
import About from './views/About';
import Album from './views/Album';
import Track from './views/Track';

import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    
    // check url to see if page is a track
    const url = window.location.href.split('/');
    const pop = url.pop();
    const isTrack = (pop!=='album' && url.includes('album')) ? 1 : 0;

    // if it is assign it to state.track
    this.state = {
      track: (isTrack) ? this.props.album[pop-1] : null
    };
    
    this.onItemClick = this.onItemClick.bind(this);
  }
  
  onItemClick(item, e) {
    this.setState({track:item});
  }

  render() {
    console.log(this.state);

    return (
      <Router>
        <div className="app">
          <Header />
        
          <div className="container my-5">
            <Route path="/" component={Home} exact />

            <Route path="/about" component={About} />
      
            <Route path='/album' render={()=><Album album={this.props.album} onItemClick={this.onItemClick} />} exact />

            <Route path='/album/:track' render={()=><Track track={this.state.track} />} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
