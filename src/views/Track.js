// Track.js
import React, { Component } from 'react';

class Track extends Component {
  render(){
    const track = this.props.track;
    
    const res = Object.keys(track).map((k)=>{
      return (
        <li key={k}>{k} : {track[k]}</li>
      );
    });

    return (<div>
      <h1>track</h1>
      <ul>{res}</ul>
    </div>);
  }
}

export default Track;