// Album.js
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Album extends Component {
  render(){
    const list = this.props.album.map((item) => {
      return (
        <li key={item.id}>
          <Link to={"/album/"+item.id} onClick={this.props.onItemClick.bind(this, item)}>{item.title}</Link>
        </li>
      );
    });
    
    return (
      <div>
        <h1>Album</h1>
        <ul>{list}</ul>
      </div>
    );
  }
}

export default Album;